﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tasks.Models
{
    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public User(string username,string password) {
            this.Password = password;
            this.Username = username;
        }
    }
}
