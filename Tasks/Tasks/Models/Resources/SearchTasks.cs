﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tasks.Models.Resources
{
    class SearchTasks
    {
        public int Page { get; set; }
        public string Order { get; set; }
        public string Sort { get; set; }
        public string Search { get; set; }
        public int Quantity { get; set; }
    }
}
