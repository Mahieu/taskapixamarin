﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tasks.Models.Resources
{
    public class GetTasks
    {
        public int Count { get; set; }
        public IEnumerable<TaskItem> Tasks {get;set;}
    }
}
