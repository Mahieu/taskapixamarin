﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tasks.Models
{
    public class TaskItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public string Category { get; set; }
        public int Progress { get; set; }
        public string UserId { get; set; }
    }
}
