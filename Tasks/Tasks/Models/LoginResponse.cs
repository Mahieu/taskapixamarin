﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Tasks.Models
{
    public class LoginResponse
    {
        public string Token{ get; set; }
        public DateTime Expiration { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}
