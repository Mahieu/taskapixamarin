﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Xamarin.Essentials;
namespace Tasks.Models
{
    public sealed class RestClient
    {
        public string ApiURL { get; }
        private string token;
        public HttpClient Client { get;}
        private static RestClient instance = null;
        private static readonly object padlock = new object();
        public string Token
        {
            get { return token; }
            set
            {
                token = value;
                Preferences.Set("token", token);
                Client.DefaultRequestHeaders.Remove("Authorization");
                Client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            }
        }
        RestClient()
        {
            ApiURL = "https://aloadoftasks.herokuapp.com/";
            token = Preferences.Get("token", "");
            Client = new HttpClient();
            Client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            Client.DefaultRequestHeaders.Add("Accept", "application/json");
            Client.BaseAddress = new Uri(ApiURL);
        }
        public static RestClient Instance
        {
            get {
                lock (padlock)
                {
                    if (instance == null) {
                        instance = new RestClient();
                    }
                    return instance;
                }
            }
        }
    }
}
