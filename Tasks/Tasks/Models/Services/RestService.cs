﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Tasks.Models.Helpers;

namespace Tasks.Models
{
    public class RestService<T>
    {
        public string Endpoint { get; set; }
        private RestClient restClient;
        public RestService(string endPoint)
        {
            restClient = RestClient.Instance;
            Endpoint = StringExtension.UriCombine(restClient.ApiURL, endPoint);
        }
        public async Task<IEnumerable<T>> GetAll()
        {
            var response = await restClient.Client.GetAsync(Endpoint);
            return JsonConvert.DeserializeObject<IEnumerable<T>>(await response.Content.ReadAsStringAsync());
        }
        public async Task<U> GetAllAsync<U>(object parameters) {
            var tmpEndPoint = AddQuery(parameters);
            var response = await restClient.Client.GetAsync(tmpEndPoint);
            var stringContent = await response.Content.ReadAsStringAsync();
            var parsedResult = JsonConvert.DeserializeObject<U>(stringContent);
            return parsedResult;
        }
        public async Task<T> GetById(object id)
        {
            var response = await restClient.Client.GetAsync(StringExtension.UriCombine(Endpoint,id.ToString()));
            return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
        }
        public async Task<HttpResponseMessage> Delete(object id)
        {
            var response = await restClient.Client.DeleteAsync(StringExtension.UriCombine(Endpoint, id.ToString()));
            return response;
        }
        public async Task<T> Post(T entity)
        {
            var response = await restClient.Client.PostAsync(Endpoint,
                new StringContent(
                    JsonConvert.SerializeObject(entity),
                        Encoding.UTF8, "application/json"));

            return JsonConvert.DeserializeObject<T>(
                await response.Content.ReadAsStringAsync());
        }
        public async Task<HttpResponseMessage> Put(object id, T entity)
        {
            var response = await restClient.Client.PutAsync(StringExtension.UriCombine(Endpoint, id.ToString()),
                new StringContent(
                    JsonConvert.SerializeObject(entity),
                        Encoding.UTF8, "application/json"));

            return response;
        }
        private string AddQuery(object parameters) {
            string tmpEndPoint = this.Endpoint;
            if (tmpEndPoint.EndsWith("/")){
                tmpEndPoint= tmpEndPoint.Substring(0,tmpEndPoint.Length - 1);
            }
            tmpEndPoint += "?";
            foreach (var prop in parameters.GetType().GetProperties())
            {
                Console.WriteLine("{0} = {1}", prop.Name, prop.GetValue(parameters, null));
                if (prop.GetValue(parameters, null) != null) {
                    tmpEndPoint += prop.Name + "=" + prop.GetValue(parameters, null).ToString()+"&";
                }
            }
            /*foreach (PropertyInfo prop in parameters.GetType().GetProperties())
            {
                var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                tmpEndPoint += type.Name + "=" + prop.GetValue(parameters).ToString();
            }*/
            return tmpEndPoint;
        }
    }
}
