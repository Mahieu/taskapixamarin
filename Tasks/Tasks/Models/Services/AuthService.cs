﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Tasks.Models.Helpers;

namespace Tasks.Models.Services
{
    public class AuthService
    {
        public string Endpoint { get; set; }
        private RestClient restClient;
        public AuthService()
        {
            restClient = RestClient.Instance;
            Endpoint = StringExtension.UriCombine(restClient.ApiURL, "authenticate");
        }
        public async Task<LoginResponse> Login(User user) {
            var loginUri = StringExtension.UriCombine(Endpoint, "login");
            var response = await restClient.Client.PostAsync(loginUri,
                new StringContent(
                    JsonConvert.SerializeObject(user),
                        Encoding.UTF8, "application/json"));
           var result = JsonConvert.DeserializeObject<LoginResponse>(
                await response.Content.ReadAsStringAsync());
            result.StatusCode = response.StatusCode;
            return result;
        }
        public HttpResponseMessage Register(User user)
        {
            var loginUri = StringExtension.UriCombine(Endpoint, "register");
            var response = restClient.Client.PostAsync(loginUri,
                new StringContent(
                    JsonConvert.SerializeObject(user),
                        Encoding.UTF8, "application/json")).Result;
            return response;
        }
    }
}
