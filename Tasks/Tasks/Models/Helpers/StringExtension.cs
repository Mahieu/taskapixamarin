﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tasks.Models.Helpers
{
    public static class StringExtension
    {
        public static string UriCombine(this string str, string param)
        {
            if (!str.EndsWith("/"))
            {
                str = str + "/";
            }
            var uri = new Uri(str);
            return new Uri(uri, param).AbsoluteUri;
        }
    }
}
