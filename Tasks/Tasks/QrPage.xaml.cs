﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing.Net.Mobile.Forms;

namespace Tasks
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QrPage : ContentPage
    {
        public QrPage()
        {
            InitializeComponent();
        }
        private async void scanButton_Clicked(object sender, EventArgs e)
        {
            var scan = new ZXingScannerPage();
            await Navigation.PushAsync(scan);
            scan.OnScanResult += (result) => {
                Device.BeginInvokeOnMainThread(async ()=>
                {
                    await Navigation.PopAsync();
                    resultLabel.Text = result.Text;
                });
            };
        }
    }
}