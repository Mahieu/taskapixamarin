﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tasks.Models;
using Tasks.Models.Services;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tasks
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        private AuthService authService;
        public LoginPage()
        {
            authService = new AuthService();
            InitializeComponent();
        }
        public async void LoginAsync(object sender, EventArgs e)
        {
            string password = passwordEntry.Text;
            string email = emailEntry.Text;
            User user = new User(email, password);
            var res = await authService.Login(user);
            if (res.StatusCode != System.Net.HttpStatusCode.OK)
            {

            }
            else {
                RestClient.Instance.Token = res.Token;
                Preferences.Set("Expiration", res.Expiration.ToString());
                App.Current.MainPage = new NavigationPage(new TasksPage());
            }
            //RestClient.Instance.Token = res.Token;
            //await Navigation.PushAsync(new MainPage());
        }
        public async void RegisterAsync(object sender, EventArgs e) {
            App.Current.MainPage = new RegisterPage();
        }
    }
}