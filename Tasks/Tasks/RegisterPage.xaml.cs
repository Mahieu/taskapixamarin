﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tasks.Models;
using Tasks.Models.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tasks
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        private AuthService authService;
        public RegisterPage()
        {
            InitializeComponent();
            authService = new AuthService();
        }


        private void password2Entry_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (passwordEntry.Text == password2Entry.Text)
            {
                registerButton.IsEnabled = true;
            }
            else
            {
                registerButton.IsEnabled = false;
            }
        }
        private void registerButton_Clicked(object sender, EventArgs e)
        {
            User tmpUser = new User(emailEntry.Text, passwordEntry.Text);
            var res = authService.Register(tmpUser);
            if (res.IsSuccessStatusCode)
            {
                App.Current.MainPage = new LoginPage();
            }
            else { 
            
            }

        }

        private void loginButton_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new LoginPage();
        }
    }
}