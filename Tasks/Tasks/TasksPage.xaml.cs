﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tasks.Models;
using Tasks.Models.Resources;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tasks
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TasksPage : ContentPage
    {
        private RestService<TaskItem> taskService;
        private IEnumerable<TaskItem> tasks;
        private SearchTasks searchTasks = new SearchTasks {
            Sort = "dueDate",
            Order="desc"
        };
        private int count;
        public TasksPage()
        {
            taskService = new RestService<TaskItem>("taskitems");
            InitializeComponent();
        }
        protected override void OnAppearing()
        {
            loadTasks();
        }
        private async void loadTasks() {
            var res = await taskService.GetAllAsync<GetTasks>(searchTasks);
            count = res.Count;
            tasks = res.Tasks;
            listView.ItemsSource = tasks;
        }
        public void AddTaskItem(object sender, EventArgs e) {
            Navigation.PushAsync(new TaskCreatePage());
        }

        private void listView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Navigation.PushAsync(new TaskEditPage(tasks.ToList().ElementAt(e.SelectedItemIndex)));
        }

        private void scannerButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new QrPage());
        }

        private void searchbar_SearchButtonPressed(object sender, EventArgs e)
        {
            searchTasks.Search = searchbar.Text;
            loadTasks();
        }

        private void orderPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (((Picker)sender).SelectedItem) {
                case "Date":
                    searchTasks.Sort = "dueDate";
                    break;
                case "Progrès":
                    searchTasks.Sort = "progress";
                    break;
                default:
                    searchTasks.Sort = "dueDate";
                    break;
            }
            loadTasks();
        }

        private void sortPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (((Picker)sender).SelectedItem)
            {
                case "Croissant":
                    searchTasks.Order = "asc";
                    break;
                case "Décroissant":
                    searchTasks.Order = "desc";
                    break;
                default:
                    searchTasks.Order = "desc";
                    break;
            }
            loadTasks();
        }
    }
}