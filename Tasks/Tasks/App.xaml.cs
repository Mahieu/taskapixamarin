﻿using System;
using Tasks.Models;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tasks
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            #if __ANDROID__
	            // Initialize the scanner first so it can track the current context
	            MobileBarcodeScanner.Initialize (Application);
            #endif
            RestClient restClient = RestClient.Instance;
            string expiration = Preferences.Get("Expiration", DateTime.MinValue.ToString());
            if (DateTime.Parse(expiration) >= DateTime.Now)
            {
                MainPage = new NavigationPage(new TasksPage());
            }
            else
            {
                MainPage = new NavigationPage(new LoginPage());
            }
            
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
