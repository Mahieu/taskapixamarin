﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tasks.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tasks
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TaskCreatePage : ContentPage
    {
        private RestService<TaskItem> restService;
        public TaskCreatePage()
        {
            InitializeComponent();
            restService = new RestService<TaskItem>("taskitems");
        }
        public async void AddTaskItemAsync(object sender, EventArgs e) {
            TaskItem ti = new TaskItem();
            ti.Description = descriptionEntry.Text;
            ti.Title = titleEntry.Text;
            ti.Progress = (int) progressSlider.Value;
            ti.DueDate = dateEntry.Date;
            ti.Category = categoryEntry.Text;
            var res = await restService.Post(ti);
            if(res != null)
            {
                await Navigation.PopAsync();
            }
        }

        private void Slider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            Slider slider = (Slider)sender;
            progressLabel.Text = ((int)slider.Value) + " %";
        }
    }
}