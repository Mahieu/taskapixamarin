﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tasks.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tasks
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TaskEditPage : ContentPage
    {
        public TaskItem taskItem;
        private RestService<TaskItem> restService;
        public TaskEditPage(TaskItem taskItem)
        {
            this.taskItem = taskItem;
            restService = new RestService<TaskItem>("taskitems");
            InitializeComponent();
            titleEntry.Text = taskItem.Title;
            descriptionEntry.Text = taskItem.Description;
            dateEntry.Date = taskItem.DueDate;
            progressSlider.Value = taskItem.Progress;
            categoryEntry.Text = taskItem.Category;
        }

        private async void updateButton_ClickedAsync(object sender, EventArgs e)
        {
            taskItem.Title = titleEntry.Text;
            taskItem.Description = descriptionEntry.Text;
            taskItem.Progress = (int)progressSlider.Value;
            taskItem.DueDate = dateEntry.Date;
            taskItem.Category = categoryEntry.Text;
            var result = await restService.Put(taskItem.Id, taskItem);
            if(result != null)
            {
                await Navigation.PopAsync();
            }
        }
        private void Slider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            Slider slider = (Slider)sender;
            progressLabel.Text = ((int)slider.Value) + " %";
        }
        private async void deleteButton_ClickedAsync(object sender, EventArgs e)
        {
            var result = await restService.Delete(taskItem.Id);
            if (result.IsSuccessStatusCode) {
                await Navigation.PopAsync();
            }
        }
    }
}